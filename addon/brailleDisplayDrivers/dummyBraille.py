# coding: UTF-8
# brailleDisplayDrivers/dummyBraille.py
# A part of NonVisual Desktop Access (NVDA)
# This file is covered by the GNU General Public License.
# See the file COPYING for more details.
# Copyright (C) 2014, 2018 Takuya Nishimoto

from collections import OrderedDict

import braille
import brailleInput
import gui
import inputCore
import wx
from logHandler import log


class brailleViewerFrame(wx.MiniFrame):

    def __init__(self):
        super(brailleViewerFrame, self).__init__(gui.mainFrame, wx.ID_ANY, _("Dummy Braille"), style=wx.CAPTION)
        self.Bind(wx.EVT_CLOSE, self.onClose)
        self.SetFont(wx.Font(20, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "DejaVu Sans"))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.textCtrl = wx.TextCtrl(self, wx.ID_ANY, size=(520, 280), style=wx.TE_READONLY | wx.TE_MULTILINE)
        sizer.Add(self.textCtrl, proportion=1, flag=wx.EXPAND)
        sizer.Fit(self)
        self.SetSizer(sizer)
        self.Show(True)

    def onClose(self, evt):
        deactivateViewer()
        if not evt.CanVeto():
            self.Destroy()
            return
        evt.Veto()


_guiFrame = None
_isActive = False


def activateViewer():
    global _guiFrame, _isActive
    _guiFrame = brailleViewerFrame()
    _isActive = True


def updateViewer(text):
    if not _isActive:
        return
    if not isinstance(text, basestring):
        return
    if _guiFrame.FindFocus() == _guiFrame.textCtrl:
        return
    t = "\n" + text[0:24] + "\n\n" + text[24:48] + "\n\n" + text[48:72] + "\n\n" + text[72:96]
    _guiFrame.textCtrl.SetValue(t)


def deactivateViewer():
    global _guiFrame, _isActive
    if not _isActive:
        return
    _isActive = False
    _guiFrame.Destroy()
    _guiFrame = None


class BrailleDisplayDriver(braille.BrailleDisplayDriver):
    name = "dummyBraille"
    description = _(u"Dummy Braille")

    def __init__(self, port="auto"):
        super(BrailleDisplayDriver, self).__init__()
        self.numCells = 96
        activateViewer()

    def terminate(self):
        super(BrailleDisplayDriver, self).terminate()
        deactivateViewer()

    @classmethod
    def check(cls):
        return True

    @classmethod
    def getPossiblePorts(cls):
        ar = [cls.AUTOMATIC_PORT]
        return OrderedDict(ar)

    def display(self, data):
        if not data:
            return
        s = u''
        for c in data:
            s += unichr(0x2800 + c)
        updateViewer(s)

    gestureMap = inputCore.GlobalGestureMap({
        "globalCommands.GlobalCommands": {
        }
    })
